/** @format */

import { Aspect } from "@teambit/harmony"

export const HarmonyUiAppAspect = Aspect.create({
  // we need to update the extension id to not clash with local bit dev...
  id: "watheia/labs.harmony-ui-app",
})
