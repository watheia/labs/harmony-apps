/** @format */

import { createContext } from "react"

export type WaContextType = {
  /**
   * primary palette of theme (light or dark mode).
   */
  mode?: "light" | "dark"
}

export const WaContext = createContext<WaContextType>({
  mode: "light",
})
