/** @format */

import React, { useContext } from "react"
import { WaTheme } from "./wa-context.provider"
import { WaContext } from "./wa-context"

export function MockComponent() {
  const ctx = useContext(WaContext)

  return (
    <div style={{ color: ctx.mode === "light" ? "#cccccc" : "#333333" }}>
      this should be {ctx.mode}
    </div>
  )
}

export const BasicThemeUsage = () => {
  return (
    <WaTheme mode="dark">
      <MockComponent />
    </WaTheme>
  )
}
