/** @format */

import React, { ReactNode } from "react"
import { WaContext, WaContextType } from "./wa-context"

export type WaThemeProps = {
  /**
   * children to be rendered within this context.
   */
  children: ReactNode
} & WaContextType

export function WaTheme({ children, ...props }: WaThemeProps) {
  return <WaContext.Provider value={{ ...props }}>{children}</WaContext.Provider>
}
