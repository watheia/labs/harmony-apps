/** @format */

import React from "react"
import classNames from "classnames"

import {
  headingFontSize,
  textFontSize,
  headingMargins,
} from "@watheia/design.typography.text-sizes"
import { shadowTheme, lightPalette, brands } from "@watheia/design.theme.color-palette"
import { SourceSans } from "@watheia/design.typography.fonts.source-sans"
import texts from "./texts.module.scss"

/**
 * @name ThemeProvider
 * @description
 * Applies shared styles to all child components.
 *
 * This includes:
 * - Colors
 * - Headers and paragraphs font-size, margins, etc
 * - Brand font
 * - Shadows
 * - Specific brand related styles
 *
 * @example
 * <Theme>
 *  <Paragraph>I got all the base styles! yippee!</Paragraph>
 * </Theme>
 */

export function Theme(props: React.HTMLAttributes<HTMLDivElement>) {
  return (
    <>
      {/* TODO refactor into top-layout head*/}
      <SourceSans />
      <div
        {...props}
        className={classNames(
          headingFontSize,
          textFontSize,
          shadowTheme,
          lightPalette,
          brands,
          headingMargins,
          texts.defaults,
          props.className
        )}
      ></div>
    </>
  )
}
