/** @format */

import React from "react"

/**
 *
 * font-family: source-sans-pro, sans-serif;
 * font-weight: 400;
 * font-style: normal;

 *  font-family: source-sans-pro, sans-serif;
 *  font-weight: 700;
 *  font-style:normal;
 *
 *  font-family: source-sans-pro, sans-serif;
 *  font-weight: 700; font-style: italic;
 *
 *  font-family: source-sans-pro, sans-serif;
 *  font-weight: 400;
 *  font-style: italic;
 */
export const SourceSans = () => (
  <>
    <link rel="stylesheet" href="https://use.typekit.net/uqo7fnf.css" />
  </>
)
