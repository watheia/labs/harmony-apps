/** @format */

import React from "react"
import { SourceSans } from "./source-sans"

export const SourceSansFont = () => (
  <div style={{ fontFamily: "source-sans-pro" }}>
    <SourceSans />
    <h1>This is a simple text.</h1>
  </div>
)

export const WithoutFont = () => (
  <div>
    <h1>This is a simple text.</h1>
  </div>
)
