---
labels: ["watheia", "labs", "design", "typography", "montserrat", "font"]
description: "montserrat font"
---

<!-- @format -->

# Overview

Includes the branded font context `Montserrat`.
