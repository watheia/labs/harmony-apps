/** @format */

import React from "react"
import { Montserrat } from "./montserrat"

export const MontserratMonoFont = () => (
  <div style={{ fontFamily: "Montserrat" }}>
    <Montserrat />
    <h1>This is a simple text.</h1>
  </div>
)

export const WithoutFont = () => (
  <div>
    <h1>This is a simple text.</h1>
  </div>
)
