/** @format */

import React from "react"

export const Montserrat = () => (
  <>
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&amp;display=swap"
      rel="stylesheet"
    />
  </>
)
