/** @format */

import marginStyles from "./heading-margin-definition.module.scss"
const { headingMargins } = marginStyles
export { headingMargins }

import sizeDefinition from "./size-definition.module.scss"
const { headingFontSize, textFontSize } = sizeDefinition
export { headingFontSize, textFontSize }

import sizeOptions from "./text-sizes.module.scss"

const sizes = {
  /**
   * @default 12px
   */
  xxs: sizeOptions.xxs as string,
  /**
   * @default 14px
   */
  xs: sizeOptions.xs as string,
  /**
   * @default 15px
   */
  sm: sizeOptions.sm as string,
  /**
   * @default 16px
   */
  md: sizeOptions.md as string,
  /**
   * @default 18px
   */
  lg: sizeOptions.lg as string,
  /**
   * @default 20px
   */
  xl: sizeOptions.xl as string,
  /**
   * @default 24px
   */
  xxl: sizeOptions.xxl as string,
}

export const textSize = sizes
